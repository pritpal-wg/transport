<div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
    <ul class="nav navbar-nav text-uppercase">

        <li class="dropdown-fw <?= ($Page == 'home') ? 'open' : '' ?>">
            <a href='<?= make_admin_url('home'); ?>'>
                Dashboard
            </a>
        </li>
        <li class="dropdown-fw <?= ($Page == 'requests') ? 'open' : '' ?>">
            <a href='<?= make_admin_url('requests'); ?>'>
                Quote Requests
            </a>
        </li>
        <li class="dropdown-fw <?= ($Page == 'pricing') ? 'open' : '' ?>">
            <a href='<?= make_admin_url('pricing'); ?>'>
                Quote Pricing 
            </a>
        </li>
        <li class="dropdown-fw <?= ($Page == 'vehicle_type' || $Page == 'vehicle_manufacturer' || $Page == 'vehicle_model' || $Page == 'vehicle_shipping_level' || $Page == 'vehicle_carrier' || $Page == 'vehicle_distance_slab'  || $Page == 'service_level' || $Page == 'vehicle_modification' || $Page == 'running_status' || $Page == 'lead_source' || $Page == 'season') ? 'open' : '' ?>">
            <a href='<?= make_admin_url('vehicle_type'); ?>'>
                Quote Options
            </a>
        </li>
        <li class="dropdown-fw <?= ($Page == 'content') ? 'open' : '' ?>">
            <a href='<?= make_admin_url('content'); ?>'>
                Content Pages
            </a>
        </li>
        <li class="dropdown-fw <?= ($Page == 'setting') ? 'open' : '' ?>">
            <a href='<?= make_admin_url('setting'); ?>'>
                Settings
            </a>
        </li> 							
    </ul>
</div>