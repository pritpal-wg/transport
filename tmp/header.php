<!DOCTYPE html>
<!--
--- About Us  ---
Content Management System
Developed by:- cWebConsultants India
http://www.cwebconsultants.com
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>
            <?php
            if (defined('SITE_NAME')):
                echo SITE_NAME . ' | Management Panel';
            else:
                echo "Management Panel";
            endif;
            ?>
        </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>

        <!-- Data Tables -->
        <link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
        <!-- END PAGE LEVEL STYLES -->

        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN THEME STYLES -->
        <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
        <link href="assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="assets/app/app_layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="assets/app/pages/css/login.css" rel="stylesheet" type="text/css"/>
        <link href="assets/app/app_layout/css/custom.css" rel="stylesheet" type="text/css"/>

        <!-- DATE PICKER -->
        <link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
        <link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
        <link rel="stylesheet" type="text/css" href="assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>
        <link href="assets/global/css/chosen.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/css/toastr.css" rel="stylesheet" type="text/css" />

        <!-- BEGIN PAGE LEVEL STYLES for validation -->
        <link href="assets/global/plugins/validation/validationEngine.jquery.css" rel="stylesheet" type="text/css" />

        <link rel="shortcut icon" href="favicon.ico" />

    </head>

    <body class="page-header-fixed page-quick-sidebar-over-content">

        <!-- BEGIN MAIN LAYOUT -->
        <div class="wrapper">
            <!-- Header BEGIN -->
            <header class="page-header">
                <nav class="navbar mega-menu" role="navigation">
                    <div class="container-fluid">
                        <div class="clearfix navbar-fixed-top">

                            <!-- BEGIN LOGO -->
                            <a id="index" class="page-logo logo-text" href="#">
                                <?= str_replace(' ', " <span>", SITE_NAME) ?></span>
                            </a>

                            <div class="topbar-actions">

                                <!-- BEGIN USER PROFILE -->
                                <div class="btn-group-img btn-group">
                                    <img src="assets/app/app_layout/img/avatar.png" class="btn btn-sm simple-cursor" alt="Welcome <?= $admin_user->get_username(); ?>" title="Welcome <?= $admin_user->get_username(); ?>">
                                </div>
                                <!-- END USER PROFILE -->

                                <!-- BEGIN GROUP INFORMATION -->
                                <div class="btn-group-red btn-group">
                                    <a href="<?php echo make_admin_url('logout'); ?>" class="btn btn-sm" title="Logout">
                                        <i class="fa fa-lock"></i>
                                    </a>
                                </div>
                                <!-- END GROUP INFORMATION -->
                            </div>
                            <!-- END TOPBAR ACTIONS -->
                        </div>

                        <?php
                        include_once('navigation.php');
                        ?>
                    </div>
                    <!--/container-->
                </nav>
            </header>
            <!-- Header END -->		
            
            <div class="container-fluid">		
