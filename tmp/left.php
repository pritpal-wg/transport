<!-- BEGIN SIDEBAR -->
<div class="page-sidebar nav-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->        
    <ul class="page-sidebar-menu">
        <li style="margin-bottom:10px;">
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <div class="sidebar-toggler hidden-phone"></div>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        </li>

        <li class="start <?php echo ($Page == 'home') ? 'active' : '' ?> ">
            <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">
                <i class="icon-home"></i> 
                <span class="title">Dashboard</span>
                <span class="selected"></span>
            </a>
        </li>
        <li class="<?php echo ($Page == 'enquiry') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/enquiry.php'); ?>
        </li> 
        <li class="<?= ($Page == 'course' || $Page == 'subject' || $Page == 'fee_head' || $Page == 'holiday' || $Page == 'porfile') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/institute.php'); ?>
        </li>
        <li class="<?php echo ($Page == 'salary' || $Page == 'staff' || $Page == 'category' || $Page == 'designation' || $Page == 'staff_attendance') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/staff.php'); ?>
        </li>				
        <li class="<?php echo ($Page == 'session' || $Page == 'session_info') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/session.php'); ?>
        </li>  
        <li class="<?php echo ($Page == 'student' || $Page == 'view' || $Page == 'attendance' || $Page == 'exam' || $Page == 'grade' || $Page == 'fee') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/student.php'); ?>
        </li>


        <li class="<?php echo ($Page == 'vehicles') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/vehicles.php'); ?>
        </li>				
        <li class="<?php echo ($Page == 'expense' || $Page == 'income') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/account.php'); ?>
        </li> 

        <li class="<?php echo ($Page == 'certificate') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/certificate.php'); ?>
        </li>			
        <li class="<?php echo ($Page == 'report' || $Page == 'account') ? 'active' : '' ?> ">
            <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">
                <i class="icon-bar-chart"></i> 
                <span class="title">Reports</span>
                <span class="selected"></span>
            </a>
        </li>
        <li class="<?php echo ($Page == 'user') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/user.php'); ?>
        </li>	
        <li class="<?php echo ($Page == 'sms') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/sms.php'); ?>
        </li>					
        <li class="<?php echo ($Page == 'gallery' || $Page == 'notice' || $Page == 'content' || $Page == 'time' || $Page == 'social') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/website.php'); ?>
        </li> 				
        <li class="<?php echo ($Page == 'logout') ? 'active' : '' ?>">
            <a href="<?php echo make_admin_url('logout', 'list', 'list'); ?>">
                <i class="icon-lock"></i> 
                <span class="title">Logout</span>
                <span class="selected"></span>
            </a>
        </li>				
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->
<div class="page-content">

