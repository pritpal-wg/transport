(function ($) {
    $.fn.hasAttr = function (e) {
        var attr = this.attr(e);
        if (typeof attr !== typeof undefined && attr !== false)
            return true;
        return false;
    };
    $(document).ready(function () {
        $('.price-field').on('blur', function () {
            var value = $(this).val();
            value = isNaN(value) || value === '' || value === null ? 0.00 : value;
            $(this).val(parseFloat(value).toFixed(2));
        });
        $('[data-toggle="chosen"]').chosen();
        if ($('#pricing_type_id_pre').length) {
            $('#pricing_type_id_pre').change();
        }
    }).on('click', '.go2top', function (e) {
        e.preventDefault();
        $('body').scrollTop(1);
    }).on('change', '#pricing_type_id_pre', function () {
        var el = $(this);
        var pricing_type = el.attr('data-for');
        var quote_type = el.attr('data-quote_type');
        var from_edit = el.attr('data-from_edit');
        if(!from_edit)
            from_edit = 0;
        var selected_id = el.attr('data-selected_id');
        if(!selected_id)
            selected_id = 0;
        var pricing_type_id = el.val();
        var main_selector = $('#pricing_type_id[data-for="' + pricing_type + '"]');
        main_selector.html('<option value="" hidden>Please Wait...</option>');
        var pricing_type_options = sync_call(ajax_url, {
            action: "pricing_type_options",
            quote_type: quote_type,
            pricing_type: pricing_type,
            pricing_type_id: pricing_type_id,
            from_edit: from_edit,
            selected_id: selected_id,
        }, false, true);
        main_selector.html(pricing_type_options);
    }).on('click', '[data-toggle="pricing_change_status"]', function (e) {
        e.preventDefault();
        var el = $(this);
        el.prop('disabled', true);
        var status = el.attr('data-status');
        var pricing_id = el.attr('data-pricing_id');
        el.removeClass("btn-success btn-danger");
        el.find('i.fa').removeClass("fa-check fa-times");
        if (status === '1') {
            $.post(ajax_url, {
                action: "change_pricing_status",
                pricing_id: pricing_id,
                set_to: 0
            }, function () {
                el.attr('data-status', 0);
                el.addClass("btn-danger");
                el.find('i.fa').addClass("fa-times");
                el.prop('disabled', false);
            });
        } else {
            $.post(ajax_url, {
                action: "change_pricing_status",
                pricing_id: pricing_id,
                set_to: 1
            }, function () {
                el.attr('data-status', 1);
                el.addClass("btn-success");
                el.find('i.fa').addClass("fa-check");
                el.prop('disabled', false);
            });
        }
    });
    function sync_call(url, data, post_call, original_response) {
        post_call = typeof post_call === 'boolean' ? post_call : false;
        original_response = typeof original_response !== 'undefined' ? original_response : false;
        $.ajaxSetup({
            async: false
        });
        if (post_call === true) {
            if (original_response === true)
                return $.post(url, data).responseText;
            return $.parseJSON($.post(url, data).responseText);
        }
        if (original_response === true)
            return $.get(url, data).responseText;
        return $.parseJSON($.get(url, data).responseText);
    }
})(jQuery);