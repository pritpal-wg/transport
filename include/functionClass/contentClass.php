<?php

/*
 * Content Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class content extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order = 'asc', $orderby = 'position') {
        $this->InitilizeSQL();
        $this->orderby = $orderby;
//        $this->parent_id = $parent_id;
        $this->order = $order;
        parent::__construct('content');
        $this->requiredVars = array('id', 'name', 'urlname', 'page', 'short_description', 'meta_keyword', 'meta_description', 'publish_date', 'page_name', 'position', 'is_deleted', 'image', 'meta_robots_index', 'meta_robots_follow', 'include_xml_sitemap', 'sitemap_priority', 'canonical', 'parent_id', 'image');
    }

    /*
     * Create new page or update existing page
     */

    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_deleted'] = isset($this->Data['is_deleted']) ? '1' : '0';

        if ($this->Data['name'] == '') {
            $this->Data['name'] = $this->Data['page_name'];
        }

        if ($this->Data['meta_keyword'] == '') {
            $this->Data['meta_keyword'] = $this->Data['page_name'];
        }

        if ($this->Data['meta_description'] == '') {
            $this->Data['meta_description'] = $this->Data['page_name'];
        }

        if ($this->Data['urlname'] == '') {
            $this->Data['urlname'] = $this->_sanitize($this->Data['page_name']);
        }

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            isset($_POST['is_deleted']) ? $_POST['is_deleted'] : $_POST['is_deleted'] = 0;
            $rand = rand(0, 99999999);
            if (upload_photo('content', $_FILES['image'], $rand))
                $this->Data['image'] = make_file_name($_FILES['image']['name'], $rand);
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $rand = rand(0, 99999999);
            if (upload_photo('content', $_FILES['image'], $rand))
                $this->Data['image'] = make_file_name($_FILES['image']['name'], $rand);
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getItems($active = false) {
        if ($active === true)
            $this->Where = "WHERE 1=1";
        else
            $this->Where = "WHERE is_deleted=1";
        $this->Where .= " ORDER BY position ASC";
        return $this->ListOfAllRecords();
    }

    function getItem($id = 0) {
        $this->Where = "WHERE 1=1";
        if (is_numeric($id) && $id > 0) {
            $this->Where .= " AND id=$id";
            return $this->DisplayOne();
        }
        return false;
    }

    function deleteById($id) {
        $this->id = $id;
        $this->Delete();
    }

}

?>