<?php

/**
 * Do not remove the types from here, for using it on front
 */
$allowed_quote_types = array(
    'domestic',
    'international',
);
$allowed_pricing_types_labels = array(
    'service_level' => 'Service Level',
    'vehicle_type' => 'Vehicle Type',
    'vehicle_modification' => 'Vehicle Modification',
    'running_status' => 'Running Status',
    'lead_source' => 'Lead Source',
    'season' => 'Season',
    'vehicle_distance_slab' => 'Distances',
);
$allowed_pricing_types = array_keys($allowed_pricing_types_labels);

class quote_price extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($orderby = 'id', $order = 'ASC') {
        parent::__construct('quote_price');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'quote_type', 'pricing_type', 'pricing_type_id', 'deposit_flat', 'carrier_flat', 'deposit_mile', 'carrier_mile', 'updated_by_user_id', 'status');
    }

    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['status'] = isset($this->Data['status']) ? '1' : '0';
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function delete_by_id($id) {
        $this->id = $id;
        $this->Delete();
    }

    function getItems($all = false) {
        $this->Where = "WHERE 1=1";
        if ($all !== true)
            $this->Where .= " AND status=1";
        return $this->ListOfAllRecords();
    }

    function getItem($id = 0) {
        $this->Where = "WHERE 1=1";
        if (is_numeric($id) && $id > 0) {
            $this->Where .= " AND id=$id";
            return $this->DisplayOne();
        }
        return false;
    }

    function getPricing($opts = array()) {
        $all = false;
        $quote_type;
        $pricing_type;
        $pricing_type_id;
        global $allowed_quote_types, $allowed_pricing_types;
        if (is_array($opts) && !empty($opts)) {
            extract($opts);
        }
        $this->Where = "WHERE 1=1";
        if ($all !== true)
            $this->Where .= " AND status=1";
        if (!empty($quote_type) && in_array($quote_type, $allowed_quote_types)) {
            $this->Where .= " AND quote_type='$quote_type'";
            if (!empty($pricing_type) && in_array($pricing_type, $allowed_pricing_types)) {
                $this->Where .= " AND pricing_type='$pricing_type'";
                if (!empty($pricing_type_id) && is_numeric($pricing_type_id) && $pricing_type_id > 0) {
                    $this->Where .= " AND pricing_type_id='$pricing_type_id'";
                    return $this->DisplayOne();
                }
            }
        }
        return $this->ListOfAllRecords();
    }

}
