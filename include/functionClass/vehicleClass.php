<?php

class vehicle_type extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($orderby = 'id', $order = 'asc') {
        parent::__construct('vehicle_type');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'status');
    }

    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['status'] = isset($this->Data['status']) ? '1' : '0';
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getItems($all = false) {
        $this->Where = "WHERE 1=1";
        if ($all !== true)
            $this->Where .= " AND status=1";
        $this->Where .= " ORDER BY name ASC";
        return $this->ListOfAllRecords();
    }

    function getItem($id = 0) {
        $this->Where = "WHERE 1=1";
        if (is_numeric($id) && $id > 0) {
            $this->Where .= " AND id=$id";
            return $this->DisplayOne();
        }
        return false;
    }

    function delete_by_id($id) {
        $this->id = $id;
        $this->Delete();
    }

    public static function displayDifferentTableInformation($id) {
        $query = new vehicle_type;
        $query->Where = "WHERE `id` = '$id'";
        return $query->DisplayOne();
    }

    function statusUpdate($active, $id) {
        if ($active == 'active') {
            $this->Data['status'] = 0;
        } else {
            $this->Data['status'] = 1;
        }
        $this->Data['id'] = $id;
        $this->Update();
    }

}

class vehicle_manufacturer extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($orderby = 'id', $order = 'asc') {
        parent::__construct('vehicle_manufacturer');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'status');
    }

    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['status'] = isset($this->Data['status']) ? '1' : '0';
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getItems($active = false) {
        if ($active === true)
            $this->Where = "WHERE 1=1";
        else
            $this->Where = "WHERE status=1";
        $this->Where .= " ORDER BY name ASC";
        return $this->ListOfAllRecords();
    }

    function getItem($id = 0) {
        $this->Where = "WHERE 1=1";
        if (is_numeric($id) && $id > 0) {
            $this->Where .= " AND id=$id";
            return $this->DisplayOne();
        }
        return false;
    }

    function delete_by_id($id) {
        $this->id = $id;
        $this->Delete();
    }

    function statusUpdate($active, $id) {
        if ($active == 'active') {
            $this->Data['status'] = 0;
        } else {
            $this->Data['status'] = 1;
        }
        $this->Data['id'] = $id;
        $this->Update();
    }

//    public static function displayUsingDifferentId($id) {
//        $query = new vehicle_manufacturer;
//        $query->Where = "WHERE `id` = '$id'";
//        return $query->DisplayOne();
//    }
}

class vehicle_model extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($orderby = 'id', $order = 'asc') {
        parent::__construct('vehicle_model');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'model_name', 'manufacturer_id', 'type_id', 'status');
    }

    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['status'] = isset($this->Data['status']) ? '1' : '0';
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getItems($all = false) {
        $this->Where = "WHERE 1=1";
        if ($all !== true)
            $this->Where .= " AND status=1";
        $this->Where .= " ORDER BY model_name ASC";
        return $this->ListOfAllRecords();
    }

    function getItem($id = 0) {
        $this->Where = "WHERE 1=1";
        if (is_numeric($id) && $id > 0) {
            $this->Where .= " AND id=$id";
            return $this->DisplayOne();
        }
        return false;
    }

    function delete_by_id($id) {
        $this->id = $id;
        $this->Delete();
    }

    // Displaying Vehicle Model List

    function displayAllInformation() {
        $this->Field = "`vehicle_model`.`status`, `vehicle_model`.`id`, `vehicle_manufacturer`.`name`, `vehicle_type`.`name` as type_name, `vehicle_model`.`model_name`";
        $this->Where = "LEFT JOIN `vehicle_manufacturer` ON `vehicle_manufacturer`.`id` = `vehicle_model`.`manufacturer_id` LEFT JOIN `vehicle_type` ON `vehicle_type`.`id` = `vehicle_model`.`type_id` GROUP BY `vehicle_model`.`id`";
        return $this->ListOfAllRecords();
    }

    function statusUpdate($active, $id) {
        if ($active == 'active') {
            $this->Data['status'] = 0;
        } else {
            $this->Data['status'] = 1;
        }
        $this->Data['id'] = $id;
        $this->Update();
    }

}

class vehicle_shipping_level extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($orderby = 'id', $order = 'asc') {
        parent::__construct('vehicle_shipping_level');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'status');
    }

    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['status'] = isset($this->Data['status']) ? '1' : '0';
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getItems($active = false) {
        if ($active === true)
            $this->Where = "WHERE 1=1";
        else
            $this->Where = "WHERE status=1";
        $this->Where .= " ORDER BY name ASC";
        return $this->ListOfAllRecords();
    }

    function getItem($id = 0) {
        $this->Where = "WHERE 1=1";
        if (is_numeric($id) && $id > 0) {
            $this->Where .= " AND id=$id";
            return $this->DisplayOne();
        }
        return false;
    }

    function delete_by_id($id) {
        $this->id = $id;
        $this->Delete();
    }

    function statusUpdate($active, $id) {
        if ($active == 'active') {
            $this->Data['status'] = 0;
        } else {
            $this->Data['status'] = 1;
        }
        $this->Data['id'] = $id;
        $this->Update();
    }

}

class vehicle_carrier extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($orderby = 'id', $order = 'asc') {
        parent::__construct('vehicle_carrier');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'quote_type', 'status');
    }

    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['status'] = isset($this->Data['status']) ? '1' : '0';
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getItems($active = false) {
        if ($active === true)
            $this->Where = "WHERE 1=1";
        else
            $this->Where = "WHERE status=1";
        $this->Where .= " ORDER BY name ASC";
        return $this->ListOfAllRecords();
    }

    function getItem($id = 0) {
        $this->Where = "WHERE 1=1";
        if (is_numeric($id) && $id > 0) {
            $this->Where .= " AND id=$id";
            return $this->DisplayOne();
        }
        return false;
    }

    function delete_by_id($id) {
        $this->id = $id;
        $this->Delete();
    }

    function statusUpdate($active, $id) {
        if ($active == 'active') {
            $this->Data['status'] = 0;
        } else {
            $this->Data['status'] = 1;
        }
        $this->Data['id'] = $id;
        $this->Update();
    }

}

class vehicle_distance_slab extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($orderby = 'id', $order = 'asc') {
        parent::__construct('vehicle_distance_slab');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'from_distance', 'to_distance', 'status');
    }

    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['status'] = isset($this->Data['status']) ? '1' : '0';
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getItems($active = false) {
        if ($active === true)
            $this->Where = "WHERE 1=1";
        else
            $this->Where = "WHERE status=1";
        $this->Where .= " ORDER BY name ASC";
        return $this->ListOfAllRecords();
    }

    function getItem($id = 0) {
        $this->Where = "WHERE 1=1";
        if (is_numeric($id) && $id > 0) {
            $this->Where .= " AND id=$id";
            return $this->DisplayOne();
        }
        return false;
    }

    function delete_by_id($id) {
        $this->id = $id;
        $this->Delete();
    }

    function statusUpdate($active, $id) {
        if ($active == 'active') {
            $this->Data['status'] = 0;
        } else {
            $this->Data['status'] = 1;
        }
        $this->Data['id'] = $id;
        $this->Update();
    }

}

class service_level extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($orderby = 'id', $order = 'asc') {
        parent::__construct('service_level');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'status');
    }

    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['status'] = isset($POST['status']) ? $POST['status'] : '0';
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getItems($active = false) {
        if ($active === true)
            $this->Where = "WHERE 1=1";
        else
            $this->Where = "WHERE status=1";
        $this->Where .= " ORDER BY name ASC";
        return $this->ListOfAllRecords();
    }

    function getItem($id = 0) {
        $this->Where = "WHERE 1=1";
        if (is_numeric($id) && $id > 0) {
            $this->Where .= " AND id=$id";
            return $this->DisplayOne();
        }
        return false;
    }

    function delete_by_id($id) {
        $this->id = $id;
        $this->Delete();
    }

    function statusUpdate($active, $id) {
        if ($active == 'active') {
            $this->Data['status'] = 0;
        } else {
            $this->Data['status'] = 1;
        }
        $this->Data['id'] = $id;
        $this->Update();
    }

}

class vehicle_modification extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($orderby = 'id', $order = 'asc') {
        parent::__construct('vehicle_modification');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'type_id', 'status');
    }

    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['status'] = isset($POST['status']) ? $POST['status'] : '0';
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getItems($active = false) {
        if ($active === true)
            $this->Where = "WHERE 1=1";
        else
            $this->Where = "WHERE status=1";
        $this->Where .= " ORDER BY name ASC";
        return $this->ListOfAllRecords();
    }

    function getItem($id = 0) {
        $this->Where = "WHERE 1=1";
        if (is_numeric($id) && $id > 0) {
            $this->Where .= " AND id=$id";
            return $this->DisplayOne();
        }
        return false;
    }

    function delete_by_id($id) {
        $this->id = $id;
        $this->Delete();
    }

    function statusUpdate($active, $id) {
        if ($active == 'active') {
            $this->Data['status'] = 0;
        } else {
            $this->Data['status'] = 1;
        }
        $this->Data['id'] = $id;
        $this->Update();
    }

}

class running_status extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($orderby = 'id', $order = 'asc') {
        parent::__construct('running_status');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'status');
    }

    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['status'] = isset($this->Data['status']) ? '1' : '0';
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getItems($active = false) {
        if ($active === true)
            $this->Where = "WHERE 1=1";
        else
            $this->Where = "WHERE status=1";
        $this->Where .= " ORDER BY name ASC";
        return $this->ListOfAllRecords();
    }

    function getItem($id = 0) {
        $this->Where = "WHERE 1=1";
        if (is_numeric($id) && $id > 0) {
            $this->Where .= " AND id=$id";
            return $this->DisplayOne();
        }
        return false;
    }

    function delete_by_id($id) {
        $this->id = $id;
        $this->Delete();
    }

    function statusUpdate($active, $id) {
        if ($active == 'active') {
            $this->Data['status'] = 0;
        } else {
            $this->Data['status'] = 1;
        }
        $this->Data['id'] = $id;
        $this->Update();
    }

}

class lead_source extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($orderby = 'id', $order = 'asc') {
        parent::__construct('lead_source');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'status');
    }

    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['status'] = isset($this->Data['status']) ? '1' : '0';
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getItems($active = false) {
        if ($active === true)
            $this->Where = "WHERE 1=1";
        else
            $this->Where = "WHERE status=1";
        $this->Where .= " ORDER BY name ASC";
        return $this->ListOfAllRecords();
    }

    function getItem($id = 0) {
        $this->Where = "WHERE 1=1";
        if (is_numeric($id) && $id > 0) {
            $this->Where .= " AND id=$id";
            return $this->DisplayOne();
        }
        return false;
    }

    function delete_by_id($id) {
        $this->id = $id;
        $this->Delete();
    }

    function statusUpdate($active, $id) {
        if ($active == 'active') {
            $this->Data['status'] = 0;
        } else {
            $this->Data['status'] = 1;
        }
        $this->Data['id'] = $id;
        $this->Update();
    }

}

class season extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($orderby = 'id', $order = 'asc') {
        parent::__construct('season');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'start_date', 'end_date', 'status');
    }

    function save($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['status'] = isset($this->Data['status']) ? '1' : '0';
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getItems($active = false) {
        if ($active === true)
            $this->Where = "WHERE 1=1";
        else
            $this->Where = "WHERE status=1";
        $this->Where .= " ORDER BY name ASC";
        return $this->ListOfAllRecords();
    }

    function getItem($id = 0) {
        $this->Where = "WHERE 1=1";
        if (is_numeric($id) && $id > 0) {
            $this->Where .= " AND id=$id";
            return $this->DisplayOne();
        }
        return false;
    }

    function delete_by_id($id) {
        $this->id = $id;
        $this->Delete();
    }

    function statusUpdate($active, $id) {
        if ($active == 'active') {
            $this->Data['status'] = 0;
        } else {
            $this->Data['status'] = 1;
        }
        $this->Data['id'] = $id;
        $this->Update();
    }

}
