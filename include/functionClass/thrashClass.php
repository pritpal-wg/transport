<?php
/*
 * Thrash Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class thrash extends cwebc {
    
    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;
    protected $thrash;
    protected $modules;
    /*
     * 
     */
    function __construct($order='asc', $orderby='position'){
        parent::__construct('module');
    }
    
    
    /*
     * Fetch deleted itesm from all modules
     */
    
    function getThrash(){
        
        /* fetch all modules */
        $this->listModules();
        
        /* get each module */
        foreach($this->modules as $k=>$v){
           $this->TableName=$v->table_name;
           $this->Where="where is_deleted='1'";
           $this->DisplayAll();
           $this->thrash[$v->display_name]=$this->GetNumRows();          
        }
        
        $this->TableName=$v->table_name;
        $this->Where="where is_deleted='1'";
        $this->DisplayAll();
        
        
        
    }
    
    function getModuleIdByDisplayName($dn=''){
           $dn=mysql_real_escape_string($dn);
           $this->TableName='module';
           $this->Where="where display_name='$dn'";
           $object=$this->DisplayOne();
           if(is_object($object)){
               return $object->id;
           }
           return false;
    }
    
        
    /*
     * Get page by id
     */
    function getModule($id){
        return $this->_getObject('module', $id);
    }
    
	 
    /*
     * Get List of all pages in array
     */
    function listModules($show_active=0, $result_type='object'){
        //$this->enablePaging($allowPaging, $pageNo, $pageSize);
        $this->Where="where is_active='1'";
        $this->DisplayAll();
        if($this->GetNumRows()){
            while($object=$this->GetObjectFromRecord()){
                $this->modules[]=$object;
            }
        }    
    }
    
    
    function getModuleDisplayName($id){
        return $this->getPage($id)->display_name;
    }
	
   function getModuleTableName($id){
        return $this->getPage($id)->table_name;
    }
    
   function getModulePageName($id){
        return $this->getPage($id)->page_name;
    }
  
   
    
	
}
?>