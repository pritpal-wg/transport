<!-- Page Content Start -->
<div class="page-content">
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <h1>Manage Quote</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li><a href="<?= make_admin_url('content') ?>">View Quote Request</a></li>
            <li class="active">Add Quote Request</li>
        </ol>
    </div>
    <!-- END BREADCRUMBS -->

    <?php
    /* display message */
    display_message(1);
    ?>	

    <!-- Left Bar Sortcut-->
    <?php include_once(DIR_FS_SITE . '/form-template/' . $modName . '/shortcut.php'); ?>  

    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-plus" style="color:#fff"></i>Edit Content Page</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <form class="form form-horizontal validation"  method="POST" enctype="multipart/form-data" >
                            <div class="form-body">
                                <div class="row">	
                                    <div class="form-group">
                                        <label for="name" class="col-md-3 control-label padding-right-none">Name</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control validate[required]"  id="name" name="name" value="<?php echo $list->name ?>">
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label for="urlname" class="col-md-3 control-label padding-right-none">Urlname</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control validate[required]"  id="urlname" name="urlname" value="<?php echo $list->urlname ?>">
                                            <!--<span class="help-block"><code></code></span>-->
                                        </div>

                                    </div>	
                                    <div class="form-group">
                                        <label for="short_description" class="col-md-3 control-label padding-right-none">Page Content</label>
                                        <div class="col-md-7">
                                            <textarea class="form-control validate[required]"  id="editor1" name="short_description"><?php echo $list->short_description ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="is_deleted" class="col-md-3 control-label padding-right-none">Status</label>
                                        <div class="col-md-7">
                                            <input type="checkbox" name="is_deleted" value="1" <?php echo ($list->is_deleted == 1) ? 'checked' : '' ?>  />
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label for="postion" class="col-md-3 control-label padding-right-none">Position</label>
                                        <div class="col-md-7">
                                            <input type="number" name="position" class="form-control" value="<?php echo $list->position ?>" />
                                        </div>
                                    </div>
                                    <h4 class="form-section hedding_inner">SEO Information</h4>
                                    <div class="form-group">
                                        <label for="page_name" class="col-md-3 control-label padding-right-none">Meta Title</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control validate[required]"  id="page_name" name="page_name" value="<?php echo $list->page_name ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="meta_keyword" class="col-md-3 control-label padding-right-none">Meta Keywords</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control validate[required]"  id="meta_keyword" name="meta_keyword" value="<?php echo $list->meta_keyword ?>">
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label for="meta_description" class="col-md-3 control-label padding-right-none">Meta Description</label>
                                        <div class="col-md-7">
                                            <textarea class="form-control validate[required]"  id="meta_description" name="meta_description"><?php echo $list->meta_description ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="meta_robots_index" class="col-md-3 control-label padding-right-none">Meta Robots Index</label>
                                        <div class="col-md-7">
                                            <select class="form-control" name="meta_robots_index">
                                                <option <?php echo ($list->meta_robots_index == 'global') ? 'selected' : '' ?>>global</option>
                                                <option <?php echo ($list->meta_robots_index == 'index') ? 'selected' : '' ?>>index</option>
                                                <option <?php echo ($list->meta_robots_index == 'noindex') ? 'selected' : '' ?>>noindex</option>
                                            </select>
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label for="meta_robots_follow" class="col-md-3 control-label padding-right-none">Meta Robots Follow</label>
                                        <div class="col-md-7">
                                            <input type="radio" name="meta_robots_follow" value="follow" <?php echo ($list->meta_robots_follow == 'follow') ? 'checked' : '' ?> /> Follow<div class="clearfix"></div>
                                            <input type="radio" name="meta_robots_follow" value="nofollow" <?php echo ($list->meta_robots_follow == 'nofollow') ? 'checked' : '' ?>  />   No Follow
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="include_xml_sitemap" class="col-md-3 control-label padding-right-none">Include in XML Sitemap</label>
                                        <div class="col-md-7">
                                            <select class="form-control" name="include_xml_sitemap">
                                                <option <?php echo ($list->include_xml_sitemap == 'global') ? 'selected' : '' ?>>global</option>
                                                <option <?php echo ($list->include_xml_sitemap == 'include') ? 'selected' : '' ?>>include</option>
                                                <option <?php echo ($list->include_xml_sitemap == 'exclude') ? 'selected' : '' ?>>exclude</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="sitemap_priority" class="col-md-3 control-label padding-right-none">Sitemap Priority</label>
                                        <div class="col-md-7">
                                            <select class="form-control" name="sitemap_priority">
                                                <option value="global" <?php echo ($list->sitemap_priority == 'global') ? 'selected' : '' ?>>Default</option>
                                                <option value="1" <?php echo ($list->sitemap_priority == '1') ? 'selected' : '' ?>>1 - Highest Priority</option>
                                                <option value="0.9" <?php echo ($list->sitemap_priority == '0.9') ? 'selected' : '' ?>>0.9</option>
                                                <option value="0.8" <?php echo ($list->sitemap_priority == '0.8') ? 'selected' : '' ?>>0.8</option>
                                                <option value="0.7" <?php echo ($list->sitemap_priority == '0.7') ? 'selected' : '' ?>>0.7</option>
                                                <option value="0.6" <?php echo ($list->sitemap_priority == '0.6') ? 'selected' : '' ?>>0.6</option>
                                                <option value="0.5" <?php echo ($list->sitemap_priority == '0.5') ? 'selected' : '' ?>>0.5 - Medium Priority</option>
                                                <option value="0.4" <?php echo ($list->sitemap_priority == '0.4') ? 'selected' : '' ?>>0.4</option>
                                                <option value="0.3" <?php echo ($list->sitemap_priority == '0.3') ? 'selected' : '' ?>>0.3</option>
                                                <option value="0.2" <?php echo ($list->sitemap_priority == '0.2') ? 'selected' : '' ?>>0.2</option>
                                                <option value="0.1" <?php echo ($list->sitemap_priority == '0.1') ? 'selected' : '' ?>>0.1 - Lowest Priority</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="canonical" class="col-md-3 control-label padding-right-none">Canonical URL</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control validate[required]"  id="canonical" name="canonical" value="<?php echo $list->canonical ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="col-md-offset-2 col-md-10">
                                        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>" />
                                        <a href="<?php echo make_admin_url($Page, 'list', 'list'); ?>" class="btn btn-default" name="cancel" > Cancel</a>											
                                        <button type="submit" name='submit' class="btn blue">Submit</button>
                                    </div>
                                </div>
                                <div class='clear'></div>
                            </div>
                        </form>										
                    </div>
                </div>
            </div>		
        </div>
    </div>
    <!-- END PAGE CONTAINER -->
</div>
<!-- PAGE CONTENT END -->
<script src="http://cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<!--<script src="http://cdn.ckeditor.com/<version.number>/<distribution>/ckeditor.js"></script>-->

<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
</script>