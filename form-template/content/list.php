<div class="page-content">
    <div class="breadcrumbs">
        <h1>Manage Content Pages</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li class="active">View Contents Pages</li>
        </ol>
    </div>
    <?php
    display_message(1);
    ?>	
    <?php include_once(DIR_FS_SITE . '/form-template/' . $modName . '/shortcut.php'); ?>  

    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption">Content Pages</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dataTable no-footer" id="sample_1" role="grid">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Page Name</th>
                                    <th>Position</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($content_lists as $key => $content_lists) { ?>
                                    <tr>
                                        <td><?php echo $key + 1 ?></td>
                                        <td><?php echo $content_lists['name'] ?></td>
                                        <td><?php echo $content_lists['position'] ?></td>
                                        <td>
                                            <a href="<?php echo make_admin_url('content', 'update', 'update&id=' . $content_lists['id']) ?>"><i class="icon-pencil"></i></a>&nbsp;&nbsp;&nbsp;&nbsp
                                            <a href="<?php echo make_admin_url('content', 'delete', 'delete&id=' . $content_lists['id']) ?>" onclick="return confirm('Are You Sure')"><i class="icon-trash"></i></a>

                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>		
    </div>
</div>