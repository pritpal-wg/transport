<!-- Page Content Start -->
<div class="page-content">
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <h1>Manage Quote</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li><a href="<?= make_admin_url('content') ?>">View Quote Request</a></li>
            <li class="active">Add Quote Request</li>
        </ol>
    </div>
    <!-- END BREADCRUMBS -->
    <?php
    /* display message */
    display_message(1);
    ?>	

    <!-- Left Bar Sortcut-->
    <?php include_once(DIR_FS_SITE . '/form-template/' . $modName . '/shortcut.php'); ?>  

    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-plus" style="color:#fff"></i>Add New Content Page</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <form class="form form-horizontal validation"  method="POST" enctype="multipart/form-data" >
                            <div class="form-body">
                                <div class="row">	
                                    <div class="form-group">
                                        <label for="name" class="col-md-3 control-label padding-right-none">Name</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control validate[required]"  id="name" name="name">
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label for="urlname" class="col-md-3 control-label padding-right-none">Urlname</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control validate[required]"  id="urlname" name="urlname">
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label for="short_description" class="col-md-3 control-label padding-right-none">Page Content</label>
                                        <div class="col-md-7">
                                            <textarea class="form-control validate[required]"  id="editor1" name="short_description"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="is_deleted" class="col-md-3 control-label padding-right-none">Status</label>
                                        <div class="col-md-7">
                                            <input type="checkbox" name="is_deleted" value="1" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="postion" class="col-md-3 control-label padding-right-none">Position</label>
                                        <div class="col-md-7">
                                            <input type="number" class="form-control" name="position"  />
                                        </div>
                                    </div>
                                    <h4 class="form-section hedding_inner">SEO Information</h4>
                                    <div class="form-group">
                                        <label for="page_name" class="col-md-3 control-label padding-right-none">Meta Title</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control validate[required]"  id="page_name" name="page_name">
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label for="meta_keyword" class="col-md-3 control-label padding-right-none">Meta Keywords</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control validate[required]"  id="meta_keyword" name="meta_keyword">
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label for="meta_description" class="col-md-3 control-label padding-right-none">Meta Description</label>
                                        <div class="col-md-7">
                                            <textarea class="form-control validate[required]"  id="meta_description" name="meta_description"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="meta_robots_index" class="col-md-3 control-label padding-right-none">Meta Robots Index</label>
                                        <div class="col-md-7">
                                            <select class="form-control" name="meta_robots_index">
                                                <option>global</option>
                                                <option>index</option>
                                                <option>noindex</option>
                                            </select>
                                        </div>
                                    </div>	
                                    <div class="form-group">
                                        <label for="meta_robots_follow" class="col-md-3 control-label padding-right-none">Meta Robots Follow</label>
                                        <div class="col-md-7">
                                            <input type="radio" name="meta_robots_follow" value="follow" /> Follow<div class="clearfix"></div>
                                            <input type="radio" name="meta_robots_follow" value="Nofollow" />   No Follow
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="include_xml_sitemap" class="col-md-3 control-label padding-right-none">Include in XML Sitemap</label>
                                        <div class="col-md-7">
                                            <select class="form-control" name="include_xml_sitemap">
                                                <option>global</option>
                                                <option>include</option>
                                                <option>exclude</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="sitemap_priority" class="col-md-3 control-label padding-right-none">Sitemap Priority</label>
                                        <div class="col-md-7">
                                            <select class="form-control" name="sitemap_priority">
                                                <option value="global">Default</option>
                                                <option value="1">1 - Highest Priority</option>
                                                <option value="0.9">0.9</option>
                                                <option value="0.8">0.8</option>
                                                <option value="0.7">0.7</option>
                                                <option value="0.6">0.6</option>
                                                <option value="0.5">0.5 - Medium Priority</option>
                                                <option value="0.4">0.4</option>
                                                <option value="0.3">0.3</option>
                                                <option value="0.2">0.2</option>
                                                <option value="0.1">0.1 - Lowest Priority</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="canonical" class="col-md-3 control-label padding-right-none">Canonical URL</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control validate[required]"  id="canonical" name="canonical">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="col-md-offset-2 col-md-10">
                                    <a href="<?php echo make_admin_url($Page, 'list', 'list'); ?>" class="btn btn-default" name="cancel" > Cancel</a>											
                                    <button type="submit" name='submit' class="btn blue">Submit</button>
                                </div>
                            </div>
                            <div class='clear'></div>
                    </div>
                    </form>										
                </div>
            </div>
        </div>		
    </div>
</div>
<!-- END PAGE CONTAINER -->
<!-- PAGE CONTENT END -->

<!-- END PAGE SIDEBAR -->
<script src="http://cdn.ckeditor.com/4.5.4/standard/ckeditor.js"></script>
<!--<script src="http://cdn.ckeditor.com/<version.number>/<distribution>/ckeditor.js"></script>-->

<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
</script>