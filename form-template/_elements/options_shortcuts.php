<style>
    @media (min-width: 768px){
        .page-container {
            margin-left: 230px;
        }
    }
</style>
<div class="page-sidebar">
    <nav class="navbar" role="navigation" style="width: 240px">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="clearfix">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-siderbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="toggle-icon">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </span> 
            </button>
        </div>
        <div class="nav-collapse collapse navbar-collapse page-siderbar-collapse">
            <ul class="nav navbar-nav">
                <li class="<?= ($Page == 'vehicle_type') ? 'active' : '' ?>">
                    <a href="<?= make_admin_url('vehicle_type', 'list', 'list') ?>">
                        <i class="fa fa-car "></i>
                        Vehicle Type
                    </a>
                </li>
                <li class="<?= ($Page == 'vehicle_manufacturer') ? 'active' : '' ?>">
                    <a href="<?= make_admin_url('vehicle_manufacturer', 'list', 'list') ?>">
                        <i class="fa fa-building"></i>
                        Manufacturers 
                    </a>
                </li>
                <li class="<?= ($Page == 'vehicle_model') ? 'active' : '' ?>">
                    <a href="<?= make_admin_url('vehicle_model', 'list', 'list') ?>">
                        <i class="fa fa-car"></i>
                        Car Model 
                    </a>
                </li>
                <li class="<?= ($Page == 'vehicle_shipping_level') ? 'active' : '' ?>">
                    <a href="<?= make_admin_url('vehicle_shipping_level', 'list', 'list') ?>">
                        <i class="fa fa-list-alt"></i>
                        Shipping Levels 
                    </a>
                </li>
                <li class="<?= ($Page == 'vehicle_carrier') ? 'active' : '' ?>">
                    <a href="<?= make_admin_url('vehicle_carrier', 'list', 'list') ?>">
                        <i class="fa fa-truck "></i>
                        Carriers 
                    </a>
                </li>
                <li class="<?= ($Page == 'vehicle_distance_slab') ? 'active' : '' ?>">
                    <a href="<?= make_admin_url('vehicle_distance_slab', 'list', 'list') ?>">
                        <i class="fa fa-map-signs"></i>
                        Distance Levels
                    </a>
                </li>
                <li class="<?= ($Page == 'service_level') ? 'active' : '' ?>">
                    <a href="<?= make_admin_url('service_level', 'list', 'list') ?>">
                        <i class="fa fa-wrench"></i>
                        Service Levels
                    </a>
                </li>
                <li class="<?= ($Page == 'vehicle_modification') ? 'active' : '' ?>">
                    <a href="<?= make_admin_url('vehicle_modification', 'list', 'list') ?>">
                        <i class="fa fa-cab"></i>
                        Vehicle Modification
                    </a>
                </li>
                <li class="<?= ($Page == 'running_status') ? 'active' : '' ?>">
                    <a href="<?= make_admin_url('running_status', 'list', 'list') ?>">
                        <i class="fa fa-wheelchair"></i>
                        Running Status
                    </a>
                </li>
                <li class="<?= ($Page == 'lead_source') ? 'active' : '' ?>">
                    <a href="<?= make_admin_url('lead_source', 'list', 'list') ?>">
                        <i class="fa fa-lightbulb-o"></i>
                        Lead Source
                    </a>
                </li>
                <li class="<?= ($Page == 'season') ? 'active' : '' ?>">
                    <a href="<?= make_admin_url('season', 'list', 'list') ?>">
                        <i class="fa fa-sun-o"></i>
                        Season
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>