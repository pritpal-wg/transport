<div class="page-content">
    <?php include_once(DIR_FS_SITE . '/form-template/' . $modName . '/shortcut_top.php'); ?>  
    <div class="breadcrumbs">
        <!--<h1>Manage Quote Pricing</h1>-->
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li class="active">View Quote Pricing</li>
        </ol>
    </div>
    <?php display_message(1) ?>
    <div class="page-container" style="margin-left: 0;padding-left: 0">
        <h2 class="hidden-xs" style="padding-bottom: 10px;position: absolute;top: 90px">Quote Pricing<?php echo $type_label ?></h2>
        <div class="row">
            <?php
            foreach ($allowed_pricing_types as $pricing_type) {
                $file = DIR_FS_SITE . '/form-template/' . $modName . '/_elements/' . $pricing_type . '.php';
                if (file_exists($file))
                    include_once($file);
            }
            ?>
        </div>
    </div>
</div>