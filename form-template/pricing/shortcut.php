<!-- BEGIN PAGE SIDEBAR -->
<div class="page-sidebar">
    <nav class="navbar" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="clearfix">
            <!-- Toggle Button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-siderbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="toggle-icon">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </span> 
            </button>
            <!-- End Toggle Button -->
        </div>
        <div class="nav-collapse collapse navbar-collapse page-siderbar-collapse">
            <ul class="nav navbar-nav">
                <li class="<?= ($type == 'domestic') ? 'active' : '' ?>">
                    <a href="<?= make_admin_url('pricing', 'list', 'list&type=domestic') ?>">
                        <i class="fa fa-truck"></i> Domestic
                    </a>
                </li>
                <li class="<?= ($type == 'international') ? 'active' : '' ?>">
                    <a href="<?= make_admin_url('pricing', 'list', 'list&type=international') ?>">
                        <i class="fa fa-plane"></i> International
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>
<!-- END PAGE SIDEBAR -->