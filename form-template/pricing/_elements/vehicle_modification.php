<?php

$obj = new vehicle_modification;
$all_data = $obj->getItems();
$temp = array();
if (!empty($all_data)) {
    foreach ($all_data as $data) {
        $temp[$data['id']] = $data;
    }
    $all_data = $temp;
}
$label = 'Vehicle Modification';
$empty_select_option = "Select $label";
if (count($all_data) >= count(${$type . "_" . $pricing_type . "_pricings_ids"})) {
    $obj = new query('vehicle_type as vt');
    $obj->Field = "vt.*";
    $obj->Where = "INNER JOIN `vehicle_modification` as vm ON vt.id=vm.type_id";
    $obj->Where .= " WHERE vm.status=1";
    $obj->Where .= " GROUP BY vt.id";
    $vehicleTypes = $obj->ListOfAllRecords();
    $dropVehicleTypes = $vehicleTypes;
    if (!empty($vehicleTypes)) {
        $temp = array();
        foreach ($vehicleTypes as $data) {
            $temp[$data['id']] = $data;
        }
        $vehicleTypes = $temp;
        if (count($all_data) > count(${$type . "_" . $pricing_type . "_pricings_ids"})) {
            $temp = array();
            foreach ($dropVehicleTypes as $vehicleType) {
                $pricing_type_id = $vehicleType['id'];
                $obj = new query($pricing_type . ' as pt');
                $obj->Field = "pt.id,pt.name";
                $obj->Where = " WHERE pt.type_id=$pricing_type_id AND pt.status=1";
                $obj->Where .= " AND pt.id NOT IN (SELECT pricing_type_id FROM `quote_price` as qp WHERE qp.quote_type='$type' AND qp.pricing_type='$pricing_type')";
                $temp_data = $obj->ListOfAllRecords();
                if (!empty($temp_data))
                    $temp[] = $vehicleType;
            }
        }
        $dropVehicleTypes = $temp;
    }
}
include __DIR__ . '/_table.php';
?>