<?php

$obj = new season;
$all_data = $obj->getItems();
$temp = array();
if (!empty($all_data)) {
    foreach ($all_data as $data) {
        $temp[$data['id']] = $data;
    }
    $all_data = $temp;
}
$label = 'Season';
$empty_select_option = "Select {$label}";
$colspan = 2;
include __DIR__ . '/_table.php';
?>