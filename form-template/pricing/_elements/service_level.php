<?php

$obj = new service_level;
$all_data = $obj->getItems();
$temp = array();
if (!empty($all_data)) {
    foreach ($all_data as $data) {
        $temp[$data['id']] = $data;
    }
    $all_data = $temp;
}
$label = 'Service Level';
$empty_select_option = "Select $label";
include __DIR__ . '/_table.php';
?>