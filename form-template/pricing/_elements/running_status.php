<?php

$obj = new running_status;
$all_data = $obj->getItems();
$temp = array();
if (!empty($all_data)) {
    foreach ($all_data as $data) {
        $temp[$data['id']] = $data;
    }
    $all_data = $temp;
}
$label = 'Running Status';
$empty_select_option = "Select $label";
include __DIR__ . '/_table.php';
?>