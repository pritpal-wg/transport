<div class="col-md-12">
    <div class="portlet box sky-blue">
        <div class="portlet-title">
            <div class="caption"><?= $label ?></div>
        </div>
        <div class="portlet-body">
            <form method="post" enctype="multipart/form-data">
                <input type="hidden" name="quote_type" value="<?= $type ?>" />
                <input type="hidden" name="pricing_type" value="<?= $pricing_type ?>" />
                <table class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <?php if ($pricing_type == 'vehicle_distance_slab') { ?>
                                <th>Miles</th>
                            <?php } ?>
                            <?php if ($pricing_type == 'vehicle_modification') { ?>
                                <th>Modification</th>
                            <?php } ?>
                            <?php if ($pricing_type == 'season') { ?>
                                <th>Start - End</th>
                            <?php } ?>
                            <th>Deposit/Flat</th>
                            <th>Carrier/Flat</th>
                            <th>Deposit/Mile</th>
                            <th>Carrier/Mile</th>
                            <th style="width: 60px">Active</th>
                            <th style="width: 70px">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach (${$type . "_" . $pricing_type . "_pricings"} as $pricing) { ?>
                            <tr>
                                <?php if ($pricing_type == 'vehicle_modification') { ?>
                                    <td><?= $vehicleTypes[$all_data[$pricing['pricing_type_id']]['type_id']]['name'] ?></td>
                                <?php } ?>
                                <td><?= $all_data[$pricing['pricing_type_id']]['name'] ?></td>
                                <?php if ($pricing_type == 'vehicle_distance_slab') { ?>
                                    <td><?= $all_data[$pricing['pricing_type_id']]['from_distance'] ?> / <?= $all_data[$pricing['pricing_type_id']]['to_distance'] ?></td>
                                <?php } ?>
                                <?php if ($pricing_type == 'season') { ?>
                                    <td><?= $all_data[$pricing['pricing_type_id']]['start_date'] ?> - <?= $all_data[$pricing['pricing_type_id']]['end_date'] ?></td>
                                <?php } ?>
                                <td><?= number_format($pricing['deposit_flat'], 2) ?></td>
                                <td><?= number_format($pricing['carrier_flat'], 2) ?></td>
                                <td><?= number_format($pricing['deposit_mile'], 2) ?></td>
                                <td><?= number_format($pricing['carrier_mile'], 2) ?></td>
                                <th>
                                    <?php
                                    $btn_class = $pricing['status'] ? 'success' : 'danger';
                                    $icon_class = $pricing['status'] ? 'check' : 'times';
                                    ?>
                                    <button type="button" class="btn btn-xs btn-block btn-<?= $btn_class ?>" data-toggle="pricing_change_status" data-status="<?= $pricing['status'] ?>" data-pricing_id="<?= $pricing['id'] ?>">
                                        <i class="fa fa-<?= $icon_class ?>"></i> Active
                                    </button>
                                </th>
                                <th style="text-align: center">
                                    <a href="<?php echo make_admin_url('pricing', 'update', 'update', "type=$type&id=$pricing[id]") ?>" class="btn btn-xs btn-link"><i class="fa fa-edit"></i></a>
                                    <a href="<?php echo make_admin_url('pricing', 'delete', 'delete', "type=$type&id=$pricing[id]") ?>" class="btn btn-xs btn-link" onclick="return confirm('Are you sure you want to delete this Pricing');"><i class="fa fa-trash"></i></a>
                                </th>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <?php if (count($all_data) > count(${$type . "_" . $pricing_type . "_pricings_ids"})) { ?>
                        <tfoot>
                            <tr>
                                <?php if ($pricing_type == 'vehicle_modification') { ?>
                                    <th style="vertical-align: middle">
                                        <select id="pricing_type_id_pre" class="form-control input-sm" data-for="<?= $pricing_type ?>" data-quote_type="<?= $type ?>">
                                            <?php foreach ($dropVehicleTypes as $vehicleType) { ?>
                                                <option value="<?= $vehicleType['id'] ?>"><?= $vehicleType['name'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </th>
                                <?php } ?>
                                <th style="vertical-align: middle"<?= isset($colspan) ? ' colspan="' . $colspan . '"' : '' ?>>
                                    <select id="pricing_type_id" class="form-control input-sm" name="pricing_type_id" data-for="<?= $pricing_type ?>" required>
                                        <?php if ($pricing_type !== 'vehicle_modification') { ?>
                                            <option value="" hidden><?= $empty_select_option ?></option>
                                            <?php foreach ($all_data as $data) { ?>
                                                <?php
                                                if (in_array($data['id'], ${$type . "_" . $pricing_type . "_pricings_ids"}))
                                                    continue;
                                                ?>
                                                <option value="<?= $data['id'] ?>"><?= $data['name'] ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </th>
                                <th><input type="number" class="form-control input-sm price-field" id="deposit_flat" name="deposit_flat" placeholder="Deposit/Flat" min="0.00" step="any" required /></th>
                                <th><input type="number" class="form-control input-sm price-field" id="carrier_flat" name="carrier_flat" placeholder="Carrier/Flat" min="0.00" step="any" required /></th>
                                <th><input type="number" class="form-control input-sm price-field" id="deposit_mile" name="deposit_mile" placeholder="Deposit/Mile" min="0.00" step="any" required /></th>
                                <th><input type="number" class="form-control input-sm price-field" id="carrier_mile" name="carrier_mile" placeholder="Carrier/Mile" min="0.00" step="any" required /></th>
                                <th>
                                    <label>
                                        <input type="checkbox" name="status" value="1" checked /> Active
                                    </label>
                                </th>
                                <th>
                                    <button class="btn btn-sm btn-block btn-success" type="submit" name="save_pricing" value="save"><i class="fa fa-plus"></i> Add</button>
                                </th>
                            </tr>
                        </tfoot>
                    <?php } ?>
                </table>
            </form>
        </div>
    </div>
</div>