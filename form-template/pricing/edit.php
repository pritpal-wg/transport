<div class="page-content">
    <div class="breadcrumbs">
        <h1>Edit Pricing<?= $type_label . $type_sub_label ?></h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li class="active">View Quote Pricing</li>
        </ol>
    </div>
    <?php display_message(1) ?>
    <div class="page-container" style="margin-left: 0;padding-left: 0">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption">Edit Pricing - <?= $allowed_pricing_types_labels[$allowed_pricing_type] ?></div>
                    </div>
                    <div class="portlet-body">
                        <?php $pricing = (array) $item ?>
                        <form method="post" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?= $pricing['id'] ?>" />
                            <table class="table table-bordered table-condensed">
                                <thead>
                                    <tr>
                                        <th>Type</th>
                                        <?php if ($allowed_pricing_type == 'vehicle_modification') { ?>
                                            <th>Modification</th>
                                        <?php } ?>
                                        <th>Deposit/Flat</th>
                                        <th>Carrier/Flat</th>
                                        <th>Deposit/Mile</th>
                                        <th>Carrier/Mile</th>
                                        <th>Active</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php if ($allowed_pricing_type == 'vehicle_modification') { ?>
                                            <th style="vertical-align: middle">
                                                <select id="pricing_type_id_pre" class="form-control input-sm" data-for="<?= $allowed_pricing_type ?>" data-quote_type="<?= $type ?>" data-from_edit="1" data-selected_id="<?= $pricing['pricing_type_id'] ?>">
                                                    <?php foreach ($vehicleTypes as $vehicleType) { ?>
                                                        <?php
                                                        $selected = $vehicleType['id'] === $vehicleTypes[$all_data[$pricing['pricing_type_id']]['type_id']]['id'] ? ' selected' : '';
                                                        ?>
                                                        <option value="<?= $vehicleType['id'] ?>"<?= $selected ?>><?= $vehicleType['name'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </th>
                                        <?php } ?>
                                        <th style="vertical-align: middle"<?= isset($colspan) ? ' colspan="' . $colspan . '"' : '' ?>>
                                            <select id="pricing_type_id" class="form-control input-sm" name="pricing_type_id" data-for="<?= $allowed_pricing_type ?>" required>
                                                <?php if ($allowed_pricing_type !== 'vehicle_modification') { ?>
                                                    <?php foreach ($all_data as $data) { ?>
                                                        <?php $selected = $pricing['pricing_type_id'] === $data[id] ? ' selected' : '' ?>
                                                        <option value="<?= $data['id'] ?>"<?= $selected ?>><?= $data['name'] ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </th>
                                        <th><input type="number" class="form-control input-sm price-field" id="deposit_flat" name="deposit_flat" placeholder="Deposit/Flat" min="0.00" step="any" required value="<?= number_format($pricing['deposit_flat'], 2) ?>" /></th>
                                        <th><input type="number" class="form-control input-sm price-field" id="carrier_flat" name="carrier_flat" placeholder="Carrier/Flat" min="0.00" step="any" required value="<?= number_format($pricing['carrier_flat'], 2) ?>" /></th>
                                        <th><input type="number" class="form-control input-sm price-field" id="deposit_mile" name="deposit_mile" placeholder="Deposit/Mile" min="0.00" step="any" required value="<?= number_format($pricing['deposit_mile'], 2) ?>" /></th>
                                        <th><input type="number" class="form-control input-sm price-field" id="carrier_mile" name="carrier_mile" placeholder="Carrier/Mile" min="0.00" step="any" required value="<?= number_format($pricing['carrier_mile'], 2) ?>" /></th>
                                        <th>
                                            <label>
                                                <input type="checkbox" name="status" value="1"<?= $pricing['status'] ? ' checked' : '' ?> />
                                                Active
                                            </label>
                                        </th>
                                        <th>
                                            <button class="btn btn-sm btn-block btn-success" type="submit" name="update_pricing" value="update"><i class="fa fa-check"></i> Done</button>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>