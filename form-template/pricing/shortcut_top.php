<style>
    .nav>li>a:focus,
    .nav>li.active>a,
    .nav>li>a:hover {
        text-decoration: none;
        background-color: #eee;
    }
</style>
<div class="nav-collapse collapse navbar-collapse page-siderbar-collapse">
    <ul class="nav navbar-nav">
        <li class="<?= ($type == 'domestic') ? 'active' : '' ?>">
            <a href="<?= make_admin_url('pricing', 'list', 'list&type=domestic') ?>">
                <i class="fa fa-truck"></i> Domestic
            </a>
        </li>
        <li class="<?= ($type == 'international') ? 'active' : '' ?>">
            <a href="<?= make_admin_url('pricing', 'list', 'list&type=international') ?>">
                <i class="fa fa-plane"></i> International
            </a>
        </li>
    </ul>
</div>