<!-- Page Content Start -->
<div class="page-content">
	<!-- BEGIN BREADCRUMBS -->
	<div class="breadcrumbs">
		<h1>Settings</h1>
		<ol class="breadcrumb">
			<li><a href="<?=make_admin_url('home')?>">Home</a></li>
			<li class="active">Settings</li>
		</ol>
	</div>
	<!-- END BREADCRUMBS -->
	
	<?php 
	/* display message */
	display_message(1);
	?>	

	<!-- Left Bar Sortcut-->
	<?php  include_once(DIR_FS_SITE.'/form-template/'.$modName.'/shortcut.php');?>  
				

	<!-- BEGIN PAGE CONTAINER -->
	<div class="page-container">
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PORTLET-->
					<div class="portlet box sky-blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-list"></i><?=ucwords($type)?> Settings</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">
							<?php $count = count($ob);
									if($count):?>
									<br/>	
									<form action="<?php echo make_admin_url('setting', 'update', 'list');?>" method="POST" name="form_data" class="form form-horizontal validation"> 
										<?php
											foreach($ob as $kk=>$vv): ?>
												<div class="form-group">
													<label for="<?=$vv['title']?>" class="col-md-2 control-label padding-right-none"><?php echo ucfirst($vv['title']);?>:</label>
													<div class="col-md-4">
														<?php echo get_setting_control($vv['id'], $vv['type'], $vv['value']);?>
													</div>
												</div>
									  <?php endforeach;?>
											<div class="form-actions">
												<div class="col-md-offset-2 col-md-10">
													<input  type="hidden" name="type" value="<?php echo $type?>"/>
													<input class="btn green submit mt15" type="submit" name="Submit" value="Update"/>
													<div class="clear"></div>  
												</div>	
											</div>
									</form>
								  <div class="clear"></div>
								<?php endif;?>  							
						
							</div>
			</div>
				<!-- END PORTLET-->
		</div>		
		
		</div>
	
	</div>
	<!-- END PAGE CONTAINER -->
</div>
<!-- PAGE CONTENT END -->