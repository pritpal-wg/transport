<div class="page-content">
    <div class="breadcrumbs">
        <h1>Manage QUOTE OPTIONS</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li class="active">View Quote Options</li>
        </ol>
    </div>
    <?php
    display_message(1);
    ?>	
    <?php include_once(DIR_FS_SITE . '/form-template/_elements/options_shortcuts.php'); ?>   

    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list" style="color:#fff"></i>Car Model</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dataTable no-footer" id="sample_1" role="grid">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Vehicle Manufacturer</th>
                                    <th>Vehicle Type</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($list_model as $list) { ?>
                                    <tr>
                                        <td><?php echo $list['model_name']; ?></td>
                                        <td><?php echo $list['name']; ?></td>
                                        <td><?php echo $list['type_name']; ?></td>
                                        <td>
                                            <?php echo ($list['status'] == 1) ? '<a href="' . make_admin_url($Page, 'status', 'status&status=active&id=' . $list['id']) . '" class="btn btn-success btn-xs">Active</a>' : '<a href="' . make_admin_url($Page, 'status', 'status&status=inactive&id=' . $list['id']) . '" class="btn btn-danger btn-xs">Inactive</a>'; ?>

                                        </td>
                                        <td>
                                            <a href="<?php echo make_admin_url('vehicle_model', 'update', 'update&id=' . $list['id']) ?>"><i class="icon-pencil"></i></a>&nbsp;&nbsp;&nbsp;&nbsp
                                            <a href="<?php echo make_admin_url('vehicle_model', 'delete', 'delete&id=' . $list['id']) ?>" onclick="return confirm('Are You Sure')"><i class="icon-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <br />
                        <form class="validation" method="POST">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="model_name">Model Name: </label>
                                    <input type="text" class="form-control validate[required]" id="name" name="model_name"  placeholder="Enter Car Model" >
                                </div>
                                <div class="col-md-3">

                                    <label for="select_manufacturer">Select Manufacturer: </label>
                                    <select name="manufacturer_id" class="form-control">
                                        <?php foreach ($list_manufacturers as $list) { ?>
                                            <option value="<?php echo $list['id'] ?>"><?php echo $list['name'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="vehicle_type">Select Vehicle Type: </label>
                                    <select name="type_id" class="form-control">
                                        <?php foreach ($list_type as $list) { ?>
                                            <option value="<?php echo $list['id'] ?>"><?php echo $list['name'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="status">Status: </label><br />
                                    <input type="checkbox" name="status" checked />
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3">
                                    <br />

                                    <button type="submit" name='submit' class="btn blue"><i class="icon-plus"></i> Add</button>
                                </div>
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
        </div>		
    </div>
</div>