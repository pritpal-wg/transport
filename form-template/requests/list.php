<div class="page-content">
    <div class="breadcrumbs">
        <h1>Manage Quote Requests</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li class="active">View Quote Requests</li>
        </ol>
    </div>
    <?php
    display_message(1);
    ?>	
    <?php include_once(DIR_FS_SITE . '/form-template/' . $modName . '/shortcut.php'); ?>  

    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Quote Requests List</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dataTable no-footer" id="sample_1" role="grid">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Quote Number</th>
                                    <th>Date of Quote</th>
                                    <th>Payment Status</th>
                                    <th>Client Name</th>
                                    <th>Transaction ID</th>
                                    <th style="text-align: right">Amount ($)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($quote_requests as $quote_request) {
                                    $obj = new query('quote_request_user');
                                    $obj->Where = "WHERE id='$quote_request->user_id'";
                                    $user = $obj->DisplayOne();
                                    ?>
                                    <tr>
                                        <td><?php echo $i++ ?></td>
                                        <td><?php echo $quote_request->id ?></td>
                                        <td><?php echo date('Y-m-d', strtotime($quote_request->payment_date)) ?></td>
                                        <td><?php echo $quote_request->payment_status ?></td>
                                        <td><?php echo $user->first_name ?> <?php echo $user->last_name ?></td>
                                        <td><?php echo $quote_request->payment_transaction_id ?></td>
                                        <td style="text-align: right">$<?php echo number_format($quote_request->quote_option_price, 2) ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>		
    </div>
</div>