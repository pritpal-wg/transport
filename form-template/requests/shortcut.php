<!-- BEGIN PAGE SIDEBAR -->
<div class="page-sidebar">
    <nav class="navbar" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="clearfix">
            <!-- Toggle Button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-siderbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="toggle-icon">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </span> 
            </button>
            <!-- End Toggle Button -->
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="nav-collapse collapse navbar-collapse page-siderbar-collapse">

            <ul class="nav navbar-nav">
                <li class="<?= ($section == 'list') ? 'active' : '' ?>">
                    <a href="<?= make_admin_url('requests', 'list', 'list') ?>">
                        <i class="icon-list "></i>
                        VIEW QUOTE REQUEST
                    </a>
                </li>
<!--                <li class='<?= ($section == 'insert') ? 'active' : '' ?>'>
                    <a href="<?= make_admin_url('requests', 'insert', 'insert') ?>">
                        <i class="icon-plus "></i>
                        Add QUOTE REQUEST 
                    </a>
                </li>-->
            </ul>
        </div>

    </nav>
</div>
<!-- END PAGE SIDEBAR -->