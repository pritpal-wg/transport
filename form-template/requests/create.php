<!-- Page Content Start -->
<div class="page-content">
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <h1>Manage Quote</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li><a href="<?= make_admin_url('requests') ?>">View Quote Request</a></li>
            <li class="active">Add Quote Request</li>
        </ol>
    </div>
    <!-- END BREADCRUMBS -->

    <?php
    /* display message */
    display_message(1);
    ?>	

    <!-- Left Bar Sortcut-->
    <?php include_once(DIR_FS_SITE . '/form-template/' . $modName . '/shortcut.php'); ?>  


    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-plus"></i>Add New Quote</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <form class="form form-horizontal validation" action="<?php echo make_admin_url('user', 'insert', 'insert') ?>" method="POST" enctype="multipart/form-data" >
                            <div class="form-body">	
                                <div class="row">	
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="first_name" class="col-md-3 control-label padding-right-none">First Name</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control validate[required]" id="first_name" name="first_name" placeholder="First Name">
                                            </div>
                                        </div>	
                                        <div class="form-group">
                                            <label for="email" class="col-md-3 control-label padding-right-none">Email</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control validate[required,custom[email]]" id="email" name="email" placeholder="Email">
                                            </div>
                                        </div>	
                                        <div class="form-group">
                                            <label for="date_of_birth" class="col-md-3 control-label padding-right-none">D.O.B</label>
                                            <div class="col-md-7 date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                                <input type="text" class="form-control date-picker date-picker-input validate[required] " data-date-format="dd-mm-yyyy" id="date_of_birth" name="date_of_birth" placeholder="D.O.B">
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button"><i class="fa fa-calendar "></i></button>
                                                </span>
                                            </div>
                                        </div>	


                                        <div class="form-group">
                                            <label for="contact" class="col-md-3 control-label padding-right-none">Contact No.</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control validate[required,custom[integer]]" id="contact" name="contact" placeholder="Contact No.">
                                            </div>
                                        </div>											

                                        <div class="form-group">
                                            <label for="name" class="col-md-3 control-label padding-right-none">Active</label>
                                            <div class="col-md-7 padding-top-checkbox">
                                                <input type="checkbox" name='is_active' checked value='1'>
                                            </div>
                                        </div>																						
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="last_name" class="col-md-3 control-label padding-right-none">Last Name</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control validate[required]" id="last_name" name="last_name" placeholder="Last Name">
                                            </div>
                                        </div>	
                                        <div class="form-group">
                                            <label for="gender" class="col-md-3 control-label padding-right-none">Gender</label>
                                            <div class="col-md-7">
                                                <select class="form-control" name="gender" >
                                                    <option value='male'>Male</option>
                                                    <option value='female'>Female</option>
                                                </select>
                                            </div>
                                        </div>	

                                        <div class="form-group">
                                            <label for="address" class="col-md-3 control-label padding-right-none">Address</label>
                                            <div class="col-md-7">
                                                <textarea class="form-control" rows="3" name='address' placeholder="Address"></textarea>
                                            </div>
                                        </div>											
                                    </div>										
                                </div>

                            </div>
                            <div class="form-actions">
                                <div class="col-md-offset-2 col-md-10">
                                    <a href="<?php echo make_admin_url('requests', 'list', 'list'); ?>" class="btn btn-default" name="cancel" > Cancel</a>											
                                    <button type="submit" name='submit' class="btn blue">Submit</button>
                                </div>
                            </div>
                            <div class='clear'></div>
                        </form>										
                    </div>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>		

    </div>

</div>
<!-- END PAGE CONTAINER -->
</div>
<!-- PAGE CONTENT END -->