<!-- Page Content Start -->
<div class="page-content">
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <h1>Manage QUOTE OPTIONS</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li><a href="<?= make_admin_url('season') ?>">View List</a></li>
            <li class="active">Edit</li>
        </ol>
    </div>
    <!-- END BREADCRUMBS -->

    <?php
    /* display message */
    display_message(1);
    ?>	
    <!-- Left Bar Sortcut-->
    <?php include_once(DIR_FS_SITE . '/form-template/_elements/options_shortcuts.php'); ?>  
    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-pencil" style="color: #fff;"></i>Edit Vehicle Type</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form form-horizontal validation" method="POST">
                            <div class="form-body">	
                                <div class="form-group">
                                    <label for="name" class="col-md-2 control-label padding-right-none">Name</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control validate[required]" name="name" id="name" value="<?php echo $list_vehicle->name ?>" placeholder="Name" autofocus>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">	
                                <div class="form-group">
                                    <label for="start_date" class="col-md-2 control-label padding-right-none">Start Date: </label>
                                    <div class="col-md-4">
                                        <div data-date-format="dd-mm-yyyy" data-date-start-date="+0d">

                                            <input type="text" class="form-control date-picker date-picker-input validate[required] " data-date-format="yyyy-mm-dd" id="start_date" name="start_date" Value="<?php echo $list_vehicle->start_date ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">	
                                <div class="form-group">
                                    <label for="end_date" class="col-md-2 control-label padding-right-none">End Date: </label>
                                    <div class="col-md-4">
                                        <div data-date-format="dd-mm-yyyy" data-date-start-date="+0d">

                                            <input type="text" class="form-control date-picker date-picker-input validate[required] " data-date-format="yyyy-mm-dd" id="end_date" name="end_date" value="<?php echo $list_vehicle->end_date ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label padding-right-none">Status</label>
                                <div class="col-md-4 padding-top-checkbox">
                                    <input type="checkbox" name='status'  value='1' <?php echo ($list_vehicle->status == 1) ? 'checked' : '' ?> />
                                    <input type='hidden' name='id' value="<?php echo $id ?>"/>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="col-md-offset-2 col-md-10">
                                    <a href="  <?php echo make_admin_url($Page, 'list', 'list'); ?>" class="btn btn-default" name="cancel" > Cancel</a>											
                                    <button type="submit" name='submit' class="btn blue"><i class="icon-pencil"></i>   Edit</button>
                                </div>
                            </div>
                            <div class='clear'></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>		
    </div>
</div>