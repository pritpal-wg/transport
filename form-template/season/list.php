<div class="page-content">
    <div class="breadcrumbs">
        <h1>Manage QUOTE OPTIONS</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li class="active">View Quote Options</li>
        </ol>
    </div>
    <?php
    display_message(1);
    ?>	
    <?php include_once(DIR_FS_SITE . '/form-template/_elements/options_shortcuts.php'); ?>   

    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list" style="color:#fff"></i>Season</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dataTable no-footer" id="sample_1" role="grid">
                            <thead>
                                <tr>
                                    <th width="60%">Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($list_vehicles as $list) { ?>
                                    <tr>
                                        <td><?php echo $list['name']; ?></td>
                                        <td>
                                            <?php echo ($list['status'] == 1) ? '<a href="' . make_admin_url($Page, 'status', 'status&status=active&id=' . $list['id']) . '" class="btn btn-success btn-xs">Active</a>' : '<a href="' . make_admin_url($Page, 'status', 'status&status=inactive&id=' . $list['id']) . '" class="btn btn-danger btn-xs">Inactive</a>'; ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo make_admin_url('season', 'update', 'update&id=' . $list['id']) ?>"><i class="icon-pencil"></i></a>&nbsp;&nbsp;&nbsp;&nbsp
                                            <a href="<?php echo make_admin_url('season', 'delete', 'delete&id=' . $list['id']) ?>" onclick="return confirm('Are You Sure')"><i class="icon-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <br />
                        <form class="validation"  method="POST">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="vehicle_type">Enter Season: </label>
                                    <input type="text" class="form-control validate[required]" id="name" name="name"  placeholder="Enter Season" >
                                </div>
                                <div class="col-md-3" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                    <label for="start_date">Start Date: </label>
                                    <input type="text" class="form-control date-picker date-picker-input validate[required] " data-date-format="yyyy-mm-dd" id="start_date" name="start_date" placeholder="Start Date">
<!--                                    <span class="input-group-btn">
                                        <button class="btn default" type="button"><i class="fa fa-calendar "></i></button>
                                    </span>-->
                                </div>
                                <div class="col-md-3" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                    <label for="end_date">End Date: </label>
                                    <input type="text" class="form-control date-picker date-picker-input validate[required] " data-date-format="yyyy-mm-dd" id="end_date" name="end_date" placeholder="End Date">
<!--                                    <span class="input-group-btn">
                                        <button class="btn default" type="button"><i class="fa fa-calendar "></i></button>
                                    </span>-->
                                </div>
                                <div class="col-md-3">
                                    <label for="status">Status</label><br />
                                    <input type="checkbox" name="status" value="1" checked>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-3">
                                    <br />
                                    <button type="submit" name='submit' class="btn blue"><i class="icon-plus"></i> Add</button>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>		
    </div>
</div>