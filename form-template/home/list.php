<style>
    .page-sidebar .nav-collapse .navbar-nav > li > a:hover {
        color: #fff;
        background: #428bca;
    </style>
    <!-- Page Content Start -->
    <div class="page-content">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2>Dashboard</h2>
                        <hr class="star-primary">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Quote Requests</h3>
                            </div>
                            <div class="panel-body">

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Quote Pricing</h3>
                            </div>
                            <div class="panel-body">

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Quote Options</h3>
                            </div>
                            <div class="panel-body">
                                <div class="page-sidebar">
                                    <div class="nav-collapse collapse navbar-collapse page-siderbar-collapse">
                                        <ul class="nav navbar-nav">
                                            <li>
                                                <a href="<?= make_admin_url('vehicle_type', 'list', 'list') ?>">
                                                    <i class="fa fa-car"></i>
                                                    Vehicle Type
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= make_admin_url('vehicle_manufacturer', 'list', 'list') ?>">
                                                    <i class="icon-list"></i>
                                                    Manufacturers 
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= make_admin_url('vehicle_model', 'list', 'list') ?>">
                                                    <i class="fa fa-plane"></i>
                                                    Car Model 
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= make_admin_url('vehicle_shipping_level', 'list', 'list&type=vehicle_shipping_level') ?>">
                                                    <i class="fa fa-ship"></i>
                                                    Shipping Levels 
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?= make_admin_url('vehicle_carrier', 'list', 'list') ?>">
                                                    <i class="icon-list"></i>
                                                    Carriers 
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Content Pages</h3>
                            </div>
                            <div class="panel-body">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>