<!-- Page Content Start -->
<div class="page-content">
    <!-- BEGIN BREADCRUMBS -->
    <div class="breadcrumbs">
        <h1>Manage QUOTE OPTIONS</h1>
        <ol class="breadcrumb">
            <li><a href="<?= make_admin_url('home') ?>">Home</a></li>
            <li><a href="<?= make_admin_url('vehicle_modification') ?>">View List</a></li>
            <li class="active">Edit</li>
        </ol>
    </div>
    <!-- END BREADCRUMBS -->

    <?php
    /* display message */
    display_message(1);
    ?>	

    <!-- Left Bar Sortcut-->
   <?php include_once(DIR_FS_SITE . '/form-template/_elements/options_shortcuts.php'); ?>   


    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box sky-blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-pencil" style="color: #fff;"></i>Edit Car Model</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form form-horizontal validation"  method="POST">
                            <div class="form-body">	
                                <div class="form-group">

                                    <label for="model_name" class="col-md-2 control-label padding-right-none">Name</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control validate[required]" value="<?php echo $lists->name ?>" id="name" name="name" autofocus>
                                    </div>
                                    <br /><br />
                                    <label for="vehicle_type" class="col-md-2 control-label padding-right-none">Vehicle Type</label>
                                    <div class="col-md-4">
                                        <select name="type_id" class="form-control">
                                            <?php foreach ($list_type as $list) { ?>
                                                <option value="<?php echo $list['id'] ?>" <?php echo ($lists->type_id == $list['id']) ? 'selected' : '' ?>><?php echo $list['name'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <br /><br />
                                    <label for="status" class="col-md-2 control-label padding-right-none">Status</label>
                                    <div class="col-md-4">
                                        <input type="checkbox" name="status" value="1" <?php echo ($lists->status == 1) ? 'checked' : '' ?>  />
                                    </div>

                                </div>

                            </div>
                            <input type='hidden' name='id' value="<?php echo $id ?>"/>
                            <div class="form-actions">
                                <div class="col-md-offset-2 col-md-10">
                                    <a href="  <?php echo make_admin_url($Page, 'list', 'list&type=' . $type); ?>" class="btn btn-default" name="cancel" > Cancel</a>											
                                    <button type="submit" name='submit' class="btn blue"><i class="icon-pencil"></i>   Edit</button>
                                </div>
                            </div>
                            <div class='clear'></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>		
    </div>
</div>