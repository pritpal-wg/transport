<?php

$action = 'list';
if (isset($_GET['action'])) {
    if ($_GET['action']) {
        $action = trim($_GET['action'], " ");
        if (file_exists(DIR_FS_SITE . "script/ajax/_GET/$action.php")) {
            extract($_GET);
            require_once DIR_FS_SITE . "script/ajax/_GET/$action.php";
            die;
        }
    }
}
if (isset($_POST['action'])) {
    if ($_POST['action']) {
        $action = trim($_POST['action'], " ");
        if (file_exists(DIR_FS_SITE . "script/ajax/_POST/$action.php")) {
            extract($_POST);
            require_once DIR_FS_SITE . "script/ajax/_POST/$action.php";
            die;
        }
    }
}
$error = array(
    'status' => 'Error',
    'error' => 'Request Not Supported',
    'description' => 'The Current Request is not supported by the ajax Controller',
);
@header("Content-Type: text/json", TRUE);
echo json_encode($error, JSON_PRETTY_PRINT);
die; //do not remove this line
?>