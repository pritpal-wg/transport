<?php

include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';

$modName = 'vehicle_distance_slab';
switch ($action):
    case'list':
        $query = new vehicle_distance_slab;
        $list_vehicles = $query->getItems(true);

        if (isset($_POST['submit'])) {
            $query = new vehicle_distance_slab;
            $query->save($_POST);
            $admin_user->set_pass_msg('Save Successfully');
            Redirect(make_admin_url($Page, 'list', 'list'));
        }
        break;

    case'update':
        $query = new vehicle_distance_slab;
        $list_vehicle = $query->getItem($id);


        if (isset($_POST['submit'])) {
            $query = new vehicle_distance_slab;
            $query->save($_POST);
            $admin_user->set_pass_msg('Edit Successfully');
            Redirect(make_admin_url($Page, 'list', 'list'));
        }
        break;

    case'delete':
        $query = new vehicle_distance_slab;
        $query->delete_by_id($id);
        $admin_user->set_pass_msg('Delete Successfully');
        Redirect(make_admin_url($Page, 'list', 'list'));
        break;

    case'status':
        $query = new vehicle_distance_slab;
        $query->statusUpdate($_GET['status'], $id);
        $admin_user->set_pass_msg('Status Update Successfully');
        Redirect(make_admin_url($Page, 'list', 'list'));
        break;

    default:break;
endswitch;
?>
