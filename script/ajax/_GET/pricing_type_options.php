<?php

if ($pricing_type == 'vehicle_modification') {
    $obj = new query($pricing_type . ' as pt');
    $obj->Field = "pt.id,pt.name";
    $obj->Where = " WHERE pt.type_id=$pricing_type_id AND pt.status=1";
    if ($from_edit !== '1')
        $obj->Where .= " AND pt.id NOT IN (SELECT pricing_type_id FROM `quote_price` as qp WHERE qp.quote_type='$quote_type' AND qp.pricing_type='$pricing_type')";
    $data = $obj->ListOfAllRecords('object');
    if (!empty($data)) {
        foreach ($data as $e) {
            $selected = ($selected_id > 0 && $e->id == $selected_id) ? ' selected' : '';
            echo "<option value=\"$e->id\"$selected>$e->name</option>";
        }
    } else {
        echo '<option value="">No Data left for Pricing</option>';
    }
} else {
    echo '<option value="">No Data left for Pricing</option>';
}
?>