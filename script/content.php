<?php

include_once(DIR_FS_SITE . 'include/functionClass/contentClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';

$modName = 'content';
switch ($action):
    case'list':
        $query = new content;
        $content_lists = $query->getItems(true);
        break;

    case'insert':
        if (isset($_POST['submit'])) {
            $query = new content;
            $query->save($_POST);
            $admin_user->set_pass_msg('Save Successfully');
            Redirect(make_admin_url($Page, 'list', 'list'));
        }
        break;

    case'update':
        $query = new content;
        $list = $query->getItem($id);

        if (isset($_POST['submit'])) {
            $query = new content;
            $query->save($_POST);
            $admin_user->set_pass_msg('Edit Successfully');
            Redirect(make_admin_url($Page, 'list', 'list'));
        }
        break;

    case 'delete':
        $query = new content;
        $query->deleteById($id);
        $admin_user->set_pass_msg('Delete Successfully');
        Redirect(make_admin_url($Page, 'list', 'list'));
        break;

    default:break;
endswitch;
?>