<?php

include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/quoteClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
$type = isset($_GET['type']) ? $_GET['type'] : '';
$type_label = '';
if ($type == 'domestic') {
    $type_label = ' - Domestic Shipping';
}
if ($type == 'international') {
    $type_label = ' - International Shipping';
}

$modName = 'pricing';
switch ($action):
    case'list':
        if (!$type || !in_array($type, $allowed_quote_types))
            Redirect(make_admin_url("pricing", "list", "list", "type=domestic"));
        if (isset($_POST['save_pricing']) && $_POST['save_pricing'] === 'save') {
            extract($_POST);
            $error = false;
            $error_desc = array();
            if (in_array($pricing_type, $allowed_pricing_types) && in_array($quote_type, $allowed_quote_types)) {
                if ((!$pricing_type_id) &&
                        (!is_numeric($deposit_flat)) &&
                        (!is_numeric($carrier_flat)) &&
                        (!is_numeric($deposit_mile)) &&
                        (!is_numeric($carrier_mile))) {
                    $error = TRUE;
                }
                if ($error === FALSE) {
                    $obj = new quote_price;
                    $obj->save($_POST);
                    $admin_user->set_pass_msg("Pricing Saved Successfully..!!");
                }
            } else {
                $error = TRUE;
            }
            if ($error === TRUE) {
                $admin_user->set_error();
                $admin_user->set_pass_msg("Something went wrong, Please try again..!!");
            }
            Redirect(make_admin_url("pricing", "list", "list", "type=$type"));
        }
        foreach ($allowed_pricing_types as $allowed_pricing_type) {
            $var_name = "{$type}_{$allowed_pricing_type}_pricings";
            $ids_var_name = "{$var_name}_ids";
            $obj = new quote_price;
            $$var_name = $obj->getPricing(array(
                'all' => true,
                'quote_type' => $type,
                'pricing_type' => $allowed_pricing_type,
            ));
            $$ids_var_name = array();
            if (!empty($$var_name)) {
                foreach ($$var_name as $pricing) {
                    if (!in_array($pricing['pricing_type_id'], $$ids_var_name)) {
                        array_push($$ids_var_name, $pricing['pricing_type_id']);
                    }
                }
            }
        }
        break;
    case'update':
        if (!$type || !$id) {
            $admin_user->set_error();
            $admin_user->set_pass_msg("Something went wrong, Please try again..!!");
            Redirect(make_admin_url('pricing'));
        }
        $obj = new quote_price;
        $item = $obj->getItem($id);
        $allowed_pricing_type = is_object($item) ? $item->pricing_type : '';
        if (!$item || !class_exists($allowed_pricing_type)) {
            $admin_user->set_error();
            $admin_user->set_pass_msg("Invalid Quote Pricing, Please try again..!!");
            Redirect(make_admin_url('pricing', 'list', 'list', "type=$type"));
        }
        if(isset($_POST['update_pricing'])){
            $obj = new quote_price;
            $obj->save($_POST);
            $admin_user->set_pass_msg("Pricing updated Successfully..!!");
            Redirect(make_admin_url('pricing', 'list', 'list', "type=$type"));
        }
        $type_sub_label = " <small>(" . $allowed_pricing_types_labels[$allowed_pricing_type] . ")</small>";
        $obj = new $allowed_pricing_type;
        $all_data = $obj->getItems();
        $temp = array();
        if (!empty($all_data)) {
            foreach ($all_data as $data) {
                $temp[$data['id']] = $data;
            }
            $all_data = $temp;
        }
        if ($allowed_pricing_type === 'vehicle_modification') {
            $obj = new query('vehicle_type as vt');
            $obj->Field = "vt.*";
            $obj->Where = "INNER JOIN `vehicle_modification` as vm ON vt.id=vm.type_id";
            $obj->Where .= " WHERE vm.status=1";
            $obj->Where .= " GROUP BY vt.id";
            $vehicleTypes = $obj->ListOfAllRecords();
            if (!empty($vehicleTypes)) {
                $temp = array();
                foreach ($vehicleTypes as $data) {
                    $temp[$data['id']] = $data;
                }
                $vehicleTypes = $temp;
            }
        }
        break;
    case'delete':
        if (!$type || !$id) {
            $admin_user->set_error();
            $admin_user->set_pass_msg("Something went wrong, Please try again..!!");
            Redirect(make_admin_url('pricing'));
        }
        $obj = new quote_price;
        $item = $obj->getItem($id);
        if (!$item) {
            $admin_user->set_error();
            $admin_user->set_pass_msg("Invalid Quote Pricing, Please try again..!!");
            Redirect(make_admin_url('pricing', 'list', 'list', "type=$type"));
        }
        $obj = new quote_price;
        $obj->delete_by_id($id);
        $admin_user->set_pass_msg("Pricing deleted successfully..!!");
        Redirect(make_admin_url('pricing', 'list', 'list', "type=$type"));
        break;
    default:break;
endswitch;
?>