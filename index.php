<?php
require_once("include/config/config.php");

$function = array('url', 'cart', 'input', 'admin', 'users', 'gallery', 'database', 'url_rewrite');
include_functions($function);

/* check if already logged in */
if ($admin_user->is_logged_in()) {
    redirect(make_admin_url('home', 'list', 'list'));
}

/* check cookie */
if (isset($_COOKIE['admin'])) {
    $object = get_object('admin_user', $_COOKIE['admin']);
    if (is_object($object)) {
        if ($user = validate_user('admin_user', array('username' => $object->username, 'password' => $object->password))) {
            $admin_user->set_admin_user_from_object($object);
            update_last_access($user->id, 1);
            re_direct(DIR_WS_SITE_CONTROL . "control.php");
        }
    }
}

/* login user */
if (is_var_set_in_post('login')):
    if ($user = validate_user('admin_user', $_POST)):
        if (isset($_POST['remember'])) {
            setcookie('admin', $user->id, 3600 * 60, '/', SITE_NAME);
        }
        $admin_user->set_admin_user_from_object($user);
        update_last_access($user->id, 1);
        re_direct(DIR_WS_SITE_CONTROL . "control.php");
    else:
        $admin_user->set_pass_msg(MSG_LOGIN_INVALID_USERNAME_PASSWORD);
        re_direct(DIR_WS_SITE_CONTROL . 'index.php');
    endif;
endif;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<!-- 
320 and Up boilerplate extension
Andy Clarke http://about.me/malarkey
Keith Clark http://twitter.com/keithclarkcouk
Version: 2
URL: http://stuffandnonsense.co.uk/projects/320andup/
License: http://creativecommons.org/licenses/MIT
-->

<!--[if IEMobile 7]><html class="no-js iem7 oldie"><![endif]-->
<!--[if lt IE 7]><html class="no-js ie6 oldie" lang="en"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js ie7 oldie" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js ie8 oldie" lang="en"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"><!--<![endif]-->
    <!--[if (gte IE 9)|(gt IEMobile 7)]><!--><html class="no-js" lang="en"><!--<![endif]-->

        <head>
            <meta charset="utf-8" />
            <title><?= SITE_NAME ?> :: Control Panel</title>
            <meta content="width=device-width, initial-scale=1.0" name="viewport" />
            <meta name="Developers" content="WebGarh Solutions, Chandigarh"/>

            <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
                <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
                <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
                <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
                <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
                <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
                <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
                <!-- END GLOBAL MANDATORY STYLES -->

                <!-- END PAGE STYLES -->
                <!-- BEGIN THEME STYLES -->
                <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
                <link href="assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
                <link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
                <link href="assets/app/app_layout/css/layout.css" rel="stylesheet" type="text/css"/>
                <link href="assets/app/pages/css/login.css" rel="stylesheet" type="text/css"/>
                <link href="assets/app/app_layout/css/custom.css" rel="stylesheet" type="text/css"/>

                <link rel="shortcut icon" href="favicon.ico" />
        </head>
        <!-- END HEAD -->
        <!-- BEGIN BODY -->
        <body class="login">
            <!-- BEGIN LOGO -->
            <div class="logo" style="color:white; font-weight:bold;font-size:20px">
                <?php /* echo SITE_NAME */ ?>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN LOGIN -->
            <div class="content">
                <!-- BEGIN LOGIN FORM -->
                <form class="form-vertical " method="post" name="form_login" id="validation">
                    <div style='margin-bottom:10px;text-align:center;'>
                        <h3 class="form-title" ><?= SITE_NAME ?></h3>
                    </div>		  
                    <div class='login-form'>
                        <?php display_message(1); ?>
                        <div class="alert alert-error hide">
                            <button class="close" data-dismiss="alert"></button>
                            <span>Enter any username and password.</span>
                        </div>

                        <div class="control-group">
                            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                            <label class="control-label visible-ie8 visible-ie9">Username</label>
                            <div class="controls">
                                <div class="input-icon left">
                                    <i class="icon-user"></i>
                                    <input class="m-wrap placeholder-no-fix validate[required]" type="text" id="username" name="username" placeholder="User Name">
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label visible-ie8 visible-ie9">Password</label>
                            <div class="controls">
                                <div class="input-icon left">
                                    <i class="icon-lock"></i>
                                    <input class="m-wrap placeholder-no-fix validate[required]" type="password" id="password" name="password" placeholder="Password">
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <label class="checkbox">
                                <div class="" style='float:left;'><span><input type="checkbox" name="remember" value="1"></span></div> Remember me
                            </label>

                            <button type="submit" name="login" value="Login" class="btn blue pull-right" id="login_button">
                                Login <i class="m-icon-swapright m-icon-white"></i>
                            </button>            
                        </div>

                        <!--	
                        <div style="margin-top:15px" class="forget-password">
                                <h4>Forgot your password ?</h4>
                                <p>
                                    <a href="javascript:void(0)" class="" id="forget-password">click here to reset your password.</a>
                                        
                                </p>
                        </div>
                        -->
                    </div>	
                    <!-- END LOGIN FORM -->        
                    <!-- BEGIN FORGOT PASSWORD FORM -->
                    <div class="forget-form">

                        <p>Enter your e-mail address below to reset your password.</p>
                        <div class="control-group">
                            <div class="controls">
                                <div class="input-icon left">
                                    <i class="icon-envelope"></i>
                                    <input class="m-wrap placeholder-no-fix validate[required,custom[email]]" type="text" id="email" name="email" placeholder="Email">
                                </div>

                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="button" id="back-btn" class="btn">
                                <i class="m-icon-swapleft"></i> Back
                            </button>

                            <button name="forgot_pass" type="submit" class="btn blue pull-right">
                                Submit <i class="m-icon-swapright m-icon-white"></i>
                            </button>            
                        </div>
                    </div>
                    <!-- END FORGOT PASSWORD FORM -->		
                </form>

                <p class="copyright">
                    <?php echo date('Y') ?> &copy; <?php echo SITE_NAME ?> - Management Panel.
                </p>		
            </div>




            <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
            <!-- BEGIN CORE PLUGINS -->
            <!--[if lt IE 9]>
            <script src="assets/global/plugins/respond.min.js"></script>
            <script src="assets/global/plugins/excanvas.min.js"></script> 
            <![endif]-->
            <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
            <!-- END CORE PLUGINS -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript" ></script>
            <script src="assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript" ></script>
            <script src="assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript" ></script>
            <script src="assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
            <script src="assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
            <script src="assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
            <script src="assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->

            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="assets/global/scripts/metronic.js" type="text/javascript"></script>
            <script src="assets/app/app_layout/scripts/layout.js" type="text/javascript"></script>
            <script src="assets/app/app_layout/scripts/quick-sidebar.js" type="text/javascript"></script>
            <script src="assets/app/app_layout/scripts/index.js" type="text/javascript"></script>
            <script src="assets/app/pages/scripts/tasks.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL SCRIPTS -->
            <script>
                jQuery(document).ready(function () {
                    Metronic.init(); // init metronic core componets
                    Layout.init(); // init layout
                    QuickSidebar.init(); // init quick sidebar
                    Index.init(); // init index page
                    Tasks.initDashboardWidget(); // init tash dashboard widget
                });
            </script>
            <!-- END JAVASCRIPTS -->

        </body>
        <!-- END BODY -->
    </html>

